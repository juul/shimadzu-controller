EESchema Schematic File Version 4
LIBS:shimadzu_spectrophotometer_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM358 U?
U 1 1 5CE7742E
P 2700 4350
F 0 "U?" H 2700 4717 50  0000 C CNN
F 1 "LM358" H 2700 4626 50  0000 C CNN
F 2 "" H 2700 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2700 4350 50  0001 C CNN
	1    2700 4350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U?
U 3 1 5CE77434
P 2700 4350
F 0 "U?" H 2658 4396 50  0000 L CNN
F 1 "LM358" H 2658 4305 50  0000 L CNN
F 2 "" H 2700 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2700 4350 50  0001 C CNN
	3    2700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 4450 2400 4700
Wire Wire Line
	2400 4700 3000 4700
Wire Wire Line
	3000 4700 3000 4350
$Comp
L shimadzu_spectrophotometer_controller-rescue:AD549-shimadzu_spectrophotometer_controller U?
U 1 1 5CE79A64
P 1650 2200
F 0 "U?" H 1991 2246 50  0000 L CNN
F 1 "AD549" H 1991 2155 50  0000 L CNN
F 2 "" H 1650 2200 50  0001 C CNN
F 3 "~" H 1650 2200 50  0001 C CNN
	1    1650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2350 1550 2300
$Comp
L shimadzu_spectrophotometer_controller-rescue:AD549-shimadzu_spectrophotometer_controller U?
U 1 1 5CE7AA9A
P 1650 1250
F 0 "U?" H 1991 1296 50  0000 L CNN
F 1 "AD549" H 1991 1205 50  0000 L CNN
F 2 "" H 1650 1250 50  0001 C CNN
F 3 "~" H 1650 1250 50  0001 C CNN
	1    1650 1250
	1    0    0    -1  
$EndComp
Wire Notes Line width 8 rgb(194, 28, 0)
	500  2900 4500 2900
Wire Notes Line width 8 rgb(194, 27, 0)
	4500 2900 4500 500 
Text Notes 550  2850 0    35   ~ 0
This part is on the original Shimadzu photo-detector PCB
Wire Wire Line
	1950 1250 4200 1250
Wire Wire Line
	1950 2200 2550 2200
Wire Wire Line
	3500 2050 2800 2050
Wire Wire Line
	1750 2050 1750 1900
Wire Wire Line
	1750 1900 1550 1900
Wire Wire Line
	1550 2500 3000 2500
Wire Wire Line
	3000 2500 3000 1850
Wire Wire Line
	3000 1850 3500 1850
Wire Wire Line
	1550 1550 2200 1550
Wire Wire Line
	2200 1550 2200 1850
Wire Wire Line
	2200 1850 3000 1850
Wire Wire Line
	1550 950  2800 950 
Wire Wire Line
	2800 950  2800 2050
Wire Wire Line
	2800 2050 1750 2050
Wire Wire Line
	5050 2050 3700 2050
Wire Wire Line
	5200 1950 3700 1950
Wire Wire Line
	5350 1850 3700 1850
$Comp
L power:GND #PWR?
U 1 1 5CE88A35
P 5300 3450
F 0 "#PWR?" H 5300 3200 50  0001 C CNN
F 1 "GND" H 5305 3277 50  0000 C CNN
F 2 "" H 5300 3450 50  0001 C CNN
F 3 "" H 5300 3450 50  0001 C CNN
	1    5300 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5CE892DC
P 5000 3500
F 0 "#PWR?" H 5000 3350 50  0001 C CNN
F 1 "+5V" V 5015 3628 50  0000 L CNN
F 2 "" H 5000 3500 50  0001 C CNN
F 3 "" H 5000 3500 50  0001 C CNN
	1    5000 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5000 3500 5100 3500
Wire Wire Line
	5100 3500 5100 3100
$Comp
L Device:R R?
U 1 1 5CE91085
P 3150 3700
F 0 "R?" H 3220 3746 50  0000 L CNN
F 1 "33K" H 3220 3655 50  0000 L CNN
F 2 "" V 3080 3700 50  0001 C CNN
F 3 "~" H 3150 3700 50  0001 C CNN
	1    3150 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CE9108C
P 3150 4200
F 0 "R?" H 3220 4246 50  0000 L CNN
F 1 "4.7K" H 3220 4155 50  0000 L CNN
F 2 "" V 3080 4200 50  0001 C CNN
F 3 "~" H 3150 4200 50  0001 C CNN
	1    3150 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3850 3150 3950
Wire Wire Line
	3000 4350 3150 4350
Connection ~ 3000 4350
Connection ~ 3150 3950
Wire Wire Line
	3150 3950 3150 4050
$Comp
L power:+3.3V #PWR?
U 1 1 5CE9B756
P 2600 3850
F 0 "#PWR?" H 2600 3700 50  0001 C CNN
F 1 "+3.3V" H 2615 4023 50  0000 C CNN
F 2 "" H 2600 3850 50  0001 C CNN
F 3 "" H 2600 3850 50  0001 C CNN
	1    2600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4050 2600 3850
$Comp
L power:GND #PWR?
U 1 1 5CE9CF62
P 2600 4750
F 0 "#PWR?" H 2600 4500 50  0001 C CNN
F 1 "GND" H 2605 4577 50  0000 C CNN
F 2 "" H 2600 4750 50  0001 C CNN
F 3 "" H 2600 4750 50  0001 C CNN
	1    2600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4650 2600 4750
$Comp
L Amplifier_Operational:LM358 U?
U 1 1 5CEA25E9
P 6800 2850
F 0 "U?" H 6800 3217 50  0000 C CNN
F 1 "LM358" H 6800 3126 50  0000 C CNN
F 2 "" H 6800 2850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 6800 2850 50  0001 C CNN
	1    6800 2850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U?
U 3 1 5CEA25EF
P 6800 2850
F 0 "U?" H 6758 2896 50  0000 L CNN
F 1 "LM358" H 6758 2805 50  0000 L CNN
F 2 "" H 6800 2850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 6800 2850 50  0001 C CNN
	3    6800 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2950 6500 3200
Wire Wire Line
	6500 3200 7100 3200
Wire Wire Line
	7100 3200 7100 2850
$Comp
L Device:R R?
U 1 1 5CEA25FB
P 6050 2500
F 0 "R?" H 6120 2546 50  0000 L CNN
F 1 "10K" H 6120 2455 50  0000 L CNN
F 2 "" V 5980 2500 50  0001 C CNN
F 3 "~" H 6050 2500 50  0001 C CNN
	1    6050 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CEA2601
P 6050 3000
F 0 "R?" H 6120 3046 50  0000 L CNN
F 1 "10K" H 6120 2955 50  0000 L CNN
F 2 "" V 5980 3000 50  0001 C CNN
F 3 "~" H 6050 3000 50  0001 C CNN
	1    6050 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CEA2607
P 6050 2250
F 0 "#PWR?" H 6050 2100 50  0001 C CNN
F 1 "+3.3V" H 6065 2423 50  0000 C CNN
F 2 "" H 6050 2250 50  0001 C CNN
F 3 "" H 6050 2250 50  0001 C CNN
	1    6050 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2250 6050 2350
Wire Wire Line
	6500 2750 6050 2750
Wire Wire Line
	6050 2750 6050 2650
Wire Wire Line
	6050 2850 6050 2750
Connection ~ 6050 2750
$Comp
L power:GND #PWR?
U 1 1 5CEA2612
P 6050 3300
F 0 "#PWR?" H 6050 3050 50  0001 C CNN
F 1 "GND" H 6055 3127 50  0000 C CNN
F 2 "" H 6050 3300 50  0001 C CNN
F 3 "" H 6050 3300 50  0001 C CNN
	1    6050 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3300 6050 3150
$Comp
L Device:R R?
U 1 1 5CEA2619
P 7250 2200
F 0 "R?" H 7320 2246 50  0000 L CNN
F 1 "33K" H 7320 2155 50  0000 L CNN
F 2 "" V 7180 2200 50  0001 C CNN
F 3 "~" H 7250 2200 50  0001 C CNN
	1    7250 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CEA261F
P 7250 2700
F 0 "R?" H 7320 2746 50  0000 L CNN
F 1 "4.7K" H 7320 2655 50  0000 L CNN
F 2 "" V 7180 2700 50  0001 C CNN
F 3 "~" H 7250 2700 50  0001 C CNN
	1    7250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2850 7250 2850
Connection ~ 7100 2850
$Comp
L power:+3.3V #PWR?
U 1 1 5CEA262B
P 6700 2350
F 0 "#PWR?" H 6700 2200 50  0001 C CNN
F 1 "+3.3V" H 6715 2523 50  0000 C CNN
F 2 "" H 6700 2350 50  0001 C CNN
F 3 "" H 6700 2350 50  0001 C CNN
	1    6700 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2550 6700 2350
$Comp
L power:GND #PWR?
U 1 1 5CEA2639
P 6700 3250
F 0 "#PWR?" H 6700 3000 50  0001 C CNN
F 1 "GND" H 6705 3077 50  0000 C CNN
F 2 "" H 6700 3250 50  0001 C CNN
F 3 "" H 6700 3250 50  0001 C CNN
	1    6700 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3150 6700 3250
$Comp
L shimadzu_spectrophotometer_controller-rescue:5V_to_plus_minus_12V-shimadzu_spectrophotometer_controller XA?
U 1 1 5CE77764
P 5200 2700
F 0 "XA?" H 5150 2950 50  0000 L CNN
F 1 "5V_to_plus_minus_12V" V 5350 2400 39  0000 L CNN
F 2 "" H 5200 2650 50  0001 C CNN
F 3 "" H 5200 2650 50  0001 C CNN
	1    5200 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3100 5300 3450
Wire Wire Line
	5050 2300 5050 2050
Wire Wire Line
	5200 2300 5200 1950
Wire Wire Line
	5350 2300 5350 1850
Wire Wire Line
	8250 2450 8250 2400
Text Label 4300 3950 0    35   ~ 0
TO_ADC1
Text Label 8250 2400 0    35   ~ 0
TO_ADC2
Wire Wire Line
	3150 3950 4300 3950
Wire Wire Line
	7250 2450 7250 2550
Wire Wire Line
	7250 2350 7250 2450
Connection ~ 7250 2450
Wire Wire Line
	7250 2450 8250 2450
Text Notes 7050 7100 0    50   ~ 0
Licensed under the TAPR Open Hardware License (www.tapr.org/OHL)
Connection ~ 2800 2050
Connection ~ 3000 1850
$Comp
L shimadzu_spectrophotometer_controller-rescue:IDC10-shimadzu_spectrophotometer_controller J?
U 1 1 5CF0B7CA
P 3600 1850
F 0 "J?" H 3600 1425 50  0000 C CNN
F 1 "IDC10" H 3600 1516 50  0000 C CNN
F 2 "" H 3300 1650 50  0001 C CNN
F 3 "~" H 3300 1650 50  0001 C CNN
	1    3600 1850
	1    0    0    1   
$EndComp
Wire Wire Line
	3500 1650 3600 1650
Wire Wire Line
	3600 1650 3600 1750
Wire Wire Line
	3600 1750 3700 1750
Wire Wire Line
	3600 1750 3600 1950
Wire Wire Line
	3600 1950 3700 1950
Connection ~ 3600 1750
Wire Wire Line
	3700 1850 3500 1850
Wire Wire Line
	3600 1950 3500 1950
Connection ~ 3600 1950
Wire Wire Line
	3700 2050 3500 2050
Text Notes 3300 1650 2    35   ~ 0
GND
Text Notes 3300 1950 2    35   ~ 0
GND
Text Notes 3900 1950 0    35   ~ 0
GND
Text Notes 3900 1750 0    35   ~ 0
GND
Text Notes 3900 1650 0    35   ~ 0
SIG2
Text Notes 3300 1750 2    35   ~ 0
SIG1
Text Notes 3300 1850 2    35   ~ 0
V-
Text Notes 3300 2050 2    35   ~ 0
V+
Text Notes 3900 2050 0    35   ~ 0
V+
Text Notes 3900 1850 0    35   ~ 0
V-
Connection ~ 3500 1850
Connection ~ 3500 2050
Connection ~ 3700 1850
Connection ~ 3700 1950
Connection ~ 3700 2050
Wire Wire Line
	2550 2200 2550 1750
Wire Wire Line
	2550 1750 3150 1750
Wire Wire Line
	3150 3550 3150 1750
Connection ~ 3150 1750
Wire Wire Line
	3150 1750 3500 1750
Wire Wire Line
	7250 1650 4200 1650
Wire Wire Line
	4200 1650 4200 1250
Wire Wire Line
	7250 1650 7250 2050
Wire Wire Line
	3700 1650 4200 1650
Connection ~ 4200 1650
Text Notes 3300 2350 0    31   ~ 0
Pinout as seen when looking into \nthe female plug of the ribbon cable\ncoming from the shimadzu with \nthe ribbon cable going off to the left.
Wire Notes Line style solid
	3900 2750 4050 2750
Wire Notes Line style solid
	4050 2750 4050 2450
Wire Notes Line style solid
	4050 2450 3900 2450
Text Notes 3400 2450 0    50   ~ 0
Ribbon cable
Wire Notes Line style solid
	3900 2450 3900 2750
Wire Notes Line style solid
	3450 2500 3900 2500
Wire Notes Line style solid
	3450 2550 3900 2550
Wire Notes Line style solid
	3450 2600 3900 2600
Wire Notes Line style solid
	3450 2650 3900 2650
Wire Notes Line style solid
	3450 2700 3900 2700
Text Notes 3750 2850 0    50   ~ 0
Female plug
Wire Wire Line
	2100 4250 2400 4250
Wire Wire Line
	6050 2750 5700 2750
Wire Wire Line
	5700 2750 5700 5050
Wire Wire Line
	5700 5050 2100 5050
Wire Wire Line
	2100 5050 2100 4250
$Comp
L RF_Module:ESP32-WROOM-32 U?
U 1 1 5D01D072
P 8200 4800
F 0 "U?" H 8200 6378 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 8200 6287 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 8200 3300 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 7900 4850 50  0001 C CNN
	1    8200 4800
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A?
U 1 1 5D01D6BC
P 10200 3550
F 0 "A?" H 10250 4428 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 10250 4337 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 10475 2800 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 10300 3250 50  0001 C CNN
	1    10200 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 3150 9800 3250
Wire Wire Line
	9800 3850 9800 3950
Connection ~ 9800 3950
Wire Wire Line
	9800 3950 9800 4050
$Comp
L power:+3.3V #PWR?
U 1 1 5D02AD21
P 10200 2300
F 0 "#PWR?" H 10200 2150 50  0001 C CNN
F 1 "+3.3V" H 10215 2473 50  0000 C CNN
F 2 "" H 10200 2300 50  0001 C CNN
F 3 "" H 10200 2300 50  0001 C CNN
	1    10200 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 2850 10200 2300
Wire Wire Line
	10700 3450 11000 3450
Wire Wire Line
	10700 3550 11000 3550
Wire Wire Line
	10700 3650 11000 3650
Wire Wire Line
	10700 3750 11000 3750
$Comp
L pspice:DIODE D?
U 1 1 5D032F93
P 9800 5600
F 0 "D?" H 9800 5427 50  0000 C CNN
F 1 "DIODE" H 9800 5774 50  0001 C CNN
F 2 "" H 9800 5600 50  0001 C CNN
F 3 "~" H 9800 5600 50  0001 C CNN
	1    9800 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D039409
P 8200 6300
F 0 "#PWR?" H 8200 6050 50  0001 C CNN
F 1 "GND" H 8205 6127 50  0000 C CNN
F 2 "" H 8200 6300 50  0001 C CNN
F 3 "" H 8200 6300 50  0001 C CNN
	1    8200 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D03C19E
P 10200 4500
F 0 "#PWR?" H 10200 4250 50  0001 C CNN
F 1 "GND" H 10205 4327 50  0000 C CNN
F 2 "" H 10200 4500 50  0001 C CNN
F 3 "" H 10200 4500 50  0001 C CNN
	1    10200 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D03C228
P 10400 4500
F 0 "#PWR?" H 10400 4250 50  0001 C CNN
F 1 "GND" H 10405 4327 50  0000 C CNN
F 2 "" H 10400 4500 50  0001 C CNN
F 3 "" H 10400 4500 50  0001 C CNN
	1    10400 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 4350 10400 4500
Wire Wire Line
	10200 4350 10200 4500
Wire Wire Line
	8200 6200 8200 6300
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5D045828
P 10750 6200
F 0 "J?" H 10856 6478 50  0000 C CNN
F 1 "Conn_01x04_Male" H 10856 6387 50  0000 C CNN
F 2 "" H 10750 6200 50  0001 C CNN
F 3 "~" H 10750 6200 50  0001 C CNN
	1    10750 6200
	-1   0    0    -1  
$EndComp
Text Notes 10750 6100 0    50   ~ 0
Yellow\n
Text Notes 10750 6200 0    50   ~ 0
Blue
Text Notes 10750 6300 0    50   ~ 0
Red
Text Notes 10750 6400 0    50   ~ 0
White
Text Notes 10700 3400 0    50   ~ 0
To stepper\nmotor
$Comp
L Device:D_Photo D?
U 1 1 5D033093
P 9850 5400
F 0 "D?" H 9800 5695 50  0000 C CNN
F 1 "D_Photo" H 9800 5604 50  0000 C CNN
F 2 "" H 9800 5400 50  0001 C CNN
F 3 "~" H 9800 5400 50  0001 C CNN
	1    9850 5400
	1    0    0    -1  
$EndComp
Text Notes 9950 5350 0    50   ~ 0
Stepper optical\nsensor
$EndSCHEMATC
