# Microstep cal

* 24000: 38.60
* 23000: 38.00
* 22000: 37.34
* 21000: 36.74
* 20000: 36.14
* 19000: 35.51
* 18000: 34.85
* 17000: 34.24
* 16000: 33.62
* 15000: 32.95
* 14000: 32.34
* 13000: 31.74
* 12000: 31.11
* 11000: 30.51
* 10000: 29.90
* 9000: 29.23
* 8000: 28.59
* 7000: 27.98
* 6000: 27.33
* 5000: 26.73
* 4000: 26.12
* 3000: 25.54
* 2000: 24.87
* 1000: 24.25
* 0: 23.62 (23.82)
* -1000: 22.97
* -2000: 22.34
* -3000: 21.73
* -4000: 21.09
* -5000: 20.50
* -6000: 19.87

Home offset: 816

# Calibration data

Stepper positions for different wavelength adjustments on original equipment. Used to calibrate software.

Distances measured from large black ledge where the stepper rod is mounted furthest away from the stepper motor (the side of the black ledge furthest away from the stepper motor), to the aluminum block that pushes on the round thing on the end of a lever. Measured to flat side of aluminum block (the side closest to the stepper motor).

These are for the deuterium bulb

* 0 nm: 22.30
* 10 nm: 22.48
* 20 nm: 22.74
* 30 nm: 22.85
* 40 nm: 23.10
* 50 nm: 23.29
* 60 nm: 23.48
* 70 nm: 23.72
* 80 nm: 23.90
* 90 nm: 24.12
* 100 nm: 24.33
* 110 nm: 24.51
* 120 nm: 24.70
* 130 nm: 24.91
* 140 nm: 25.13
* 150 nm: 25.33
* 160 nm: 25.54
* 170 nm: 25.74
* 180 nm: 25.93
* 190 nm: 26.13
* 200 nm: 26.32
* 210 nm: 26.50
* 220 nm: 26.68
* 230 nm: 26.86
* 240 nm: 27.07
* 250 nm: 27.27
* 260 nm: 27.42
* 270 nm: 27.63
* 280 nm: 27.82
* 290 nm: 28.03
* 300 nm: 28.20
* 310 nm: 28.41
* 320 nm: 28.63
* 330 nm: 28.86
* 340 nm: 29.07
* 350 nm: 29.25
* 360 nm: 29.45
* 370 nm: 29.69
* 380 nm: 29.86
* 390 nm: 30.06
* 400 nm: 30.30
* 450 nm: 31.24
* 460 nm: 31.45
* 500 nm: 32.24
* 550 nm: 33.24
* 600 nm: 34.24
* 650 nm: 35.27
* 700 nm: 36.27
* 750 nm: 37.26
* 790 nm: 38.06
* 799 nm: 38.20

These are for the tungsten bulb:

* 800 nm: 22.24
* 820 nm: 22.64
* 840 nm: 23.03
* 860 nm: 23.42
* 880 nm: 23.87
* 900 nm: 24.26
* 920 nm: 24.67
* 940 nm: 25.07
* 960 nm: 25.48
* 980 nm: 25.88
* 999 nm: 26.25

