
WORK IN PROGRESS. Not ready for use.

# Setup

Install dependencies:

```
npm install
```

Copy example settings file:

```
cp settings.js.example settings.js
```

Edit `settings.js` to suit your needs.

# Running

```
./bin/cmd.js
```

# ToDo

Add chart.js and chartjs-node support for plotting. This might need:

```
sudo apt install libgif-dev
```



