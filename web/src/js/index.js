
import {h, render, Component} from 'preact';
import Socket from './socket.js';

import Root from './components/Root.js';

var app = {};

function renderAll() {
  var container = document.getElementById('container');

  render((
    <div>
      <Root />
    </div>
  ), container);
}


function init() {
  app.socket = new Socket('/ws', {debug: true});

  app.socket.connect(function(err, isConnected) {
    if(err) console.error(err);    

    console.log("connected:", isConnected);

  });
  
  app.actions = require('./actions/index');

  renderAll();
  
}


init();



