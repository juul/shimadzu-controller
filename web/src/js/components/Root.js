
import { h, render, Component } from 'preact';
import Plot from './Plot.js';

export default class Root extends Component {

  constructor() {
    super();
    
    this.state = {
      interpolateMode: 'akima'
    }
  }
  
  up() {
    if(this.state.interpolateMode === 'akima') {
      this.setState({
        interpolateMode: 'lines'
      });
    } else {
      this.setState({
        interpolateMode: 'akima'
      });
    }
  }
  
  render(props, state) {

    var xvals = [
      10.421,
      20,
      30,
      40,
      50,
      60,
      70,
      80
    ];

    var yvals = [
      40,
      10.432,
      17,
      39,
      99,
      120,
      30,
      11      
    ];
    console.log("interpolateMode", this.state.interpolateMode);
    return ((
      <div>
        <Plot width="800" height="600" xvals={xvals} yvals={yvals} interpolateMode={this.state.interpolateMode} />
        <button onclick={this.up.bind(this)}>up</button>
      </div>
    ))
  }
}


