
We're probably not going to use the STM32 but here it is for historical reference.

# setup

Follow [this guide](https://stm32-base.org/guides/setup) though to install stlink you can use a package if your system has one. Otherwise do this:

```
sudo apt install cmake libusb-1.0-0 libusb-1.0-0-dev pandoc
git clone https://github.com/texane/stlink
cd stlink/
make release
cd build/Release
sudo make install
```

If the build fails and you need to fix something, run `make clean` before retrying `make release`.

# hello world

You will need the `F4` template. Edit `Makefile` changine the `DEVICE` line to:

```
DEVICE = STM32F401xE
```

You do not need to modify any jumpers or press any buttons in order to flash.

This is the modified F4 template code for making the `LD2` LED blink.

```
#include "stm32f4xx.h"

// Quick and dirty delay
static void delay (unsigned int time) {
    for (unsigned int i = 0; i < time; i++)
        for (volatile unsigned int j = 0; j < 2000; j++);
}

int main (void) {
    // Turn on the GPIOC peripheral
  //    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; // BPIO bank 

    // Put pin 5 in general purpose output mode
    // Note: The only difference here is the name of the register in the
    //       definition, both lines have the same effect.
#if defined(STM32F413xx) || \
    defined(STM32F423xx)
    GPIOA->MODER |= GPIO_MODER_MODE5_0;
#else
    GPIOA->MODER |= GPIO_MODER_MODER5_0;
#endif

    while (1) {
        // Reset the state of pin 5 to output low
#if defined(STM32F413xx) || \
    defined(STM32F423xx)
        GPIOA->BSRR = GPIO_BSRR_BR_5;
#else
        GPIOA->BSRR = GPIO_BSRR_BR5;
#endif

        delay(300);

        // Set the state of pin 5 to output high
#if defined(STM32F413xx) || \
    defined(STM32F423xx)
        GPIOA->BSRR = GPIO_BSRR_BS_5;
#else
        GPIOA->BSRR = GPIO_BSRR_BS5;
#endif

        delay(200);
    }

    // Return 0 to satisfy compiler
    return 0;
}
```

# peripheral libraries

Unfortunately the STM32 peripherial library is not using an open source license.

The Arduino STM32 libraries are mostly open source but the USB client and host libareies are under the same non-open license used for the official peripheral library.

There is what seems to be an entire truly open source peripheral library called [LibOpenCM3](https://libopencm3.org/).

